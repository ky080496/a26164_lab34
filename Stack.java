/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

/**
 *
 * @author sv
 */
class Node {

    int item;
    Node next;

    public Node() {
        next = null;
    }
}

public class Stack {

    Node top;
    private int size;

    public Stack() {
        this.top = null;
        this.size = 0;
    }

    public void push(int item) {
        Node node = new Node();
        node.item = item;

        if (this.size == 0) {
            this.top = node;
        } else {
            node.next = this.top;
            this.top = node;
        }
        this.size++;
    }

    public int pop() {
        if (this.size == 0) {
            return -1;
        }
        size--;
        int item = this.top.item;
        this.top = this.top.next;
        return item;
    }

    public int search(int item) {
        Node temp = top;
        int stt = 0;
        while (temp != null) {
            stt++;
            if (temp.item == item) {
                return stt;
            }
            temp = temp.next;

        }
        return 0;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public int numOfElement() {
        return this.size;
    }

    public void display() {
        if (isEmpty()) {
            System.out.println("Stack rỗng!");
        } else {
            Node temp = top;
            System.out.print("Các phần tử trong Stack: ");
            while (temp != null) {
                if (temp.next != null) {
                    System.out.print(temp.item + "->");
                } else {
                    System.out.print(temp.item);
                }
                temp = temp.next;
            }
            System.out.println("");
        }
    }

}
