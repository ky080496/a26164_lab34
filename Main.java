/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

/**
 *
 * @author sv
 */
public class Main {

    public static void main(String[] args) {

        Stack stack = new Stack();

        stack.push(6);
        stack.push(14);
        stack.push(19);
        stack.push(13);
        stack.push(14);
        stack.push(29);

        stack.display();
        int n = stack.numOfElement();
        System.out.println("Số phần tử của Stack: " + n);

        System.out.println("\n/*------------------------------*/");
        System.out.println("Pop n phần tử trong stack có n phần tử:");
        for (int i = 1; i <= n; i++) {
            stack.pop();
        }
        stack.display();

        System.out.println("\n/*------------------------------*/");
        System.out.print("Push lại ");
        stack.push(241);
        stack.push(530);
        stack.push(112);
        stack.push(325);
        stack.push(231);
        stack.push(221);
        stack.display();
        System.out.println("Pop 3 lần (số lần pop ít hơn số phần tử trong stack:");
        for (int i = 1; i <= 3; i++) {
            stack.pop();
        }
        stack.display();
        n = stack.numOfElement() + 5;

        System.out.println("\n/*------------------------------*/");
        System.out.println("Pop n+5 lần (số lần pop nhiều hơn số phần tử trong stack:");
        for (int i = 1; i <= n; i++) {
            stack.pop();
        }
        stack.display();

    }
}
